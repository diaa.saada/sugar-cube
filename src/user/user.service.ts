import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from './entities/user.entity';

@Injectable()
export class UserService {
  constructor(@InjectModel(User.name) private userModel: Model<User>) {}

  create(createUserDto: CreateUserDto) {
    const x: CreateUserDto = {
      name: 'Diaa',
      age: 35,
      email: 'diaa@email.com',
    };
    const createdCat = new this.userModel(x);
    return createdCat.save();
    return 'This action adds a new user';
  }

  findAll() {
    const x: CreateUserDto = {
      name: 'Diaa',
      age: 35,
      email: 'diaa@email.com',
    };
    const createdCat = new this.userModel(x);
    createdCat.save();
    return this.userModel.findOne({ name: 'Diaa' });
  }

  findOne(id: number) {
    return `This action returns a #${id} user`;
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    return `This action updates a #${id} user`;
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }
}
